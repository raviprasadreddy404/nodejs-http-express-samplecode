//step 1. import product model already registered with the applicaiton
// here sample_details is db name 

let sampleModel = require('mongoose').model('sample_details');

//create controller

var sampleschmaCtrl = {};

//define db operation


// insert operation using save query 



sampleschmaCtrl.create = (req, res) => {

    //the inserting data is comming here 

    let payload = req.body;
    console.log("payload", payload);

    // we can get the response using
    // res.json({data : "recived successfully "})

    // to insert the data into the table (or) collection we are using sampleModel.

    var sampleData = new sampleModel(req.body);


    sampleData.save().then((data) => {
        console.log('Db Saved')
        res.json(data);

    }).catch((err) => {

        res.json(err);

        console.log('db is not saved')

    })

}
// get the data from the collection

// we are using find method for getting the data ,  like select * from table_name;

sampleschmaCtrl.search = (req, res) => {

    sampleModel.find({}).then((data) => {

        console.log('data is getting.. okk')

        res.json(data);

    }).catch((err) => {

        console.log('data not  is getting.. okk')

    });

}

sampleschmaCtrl.update = (req, res) => {

    sampleModel.findOneAndRemove({"_id": "5a7c191a61a28737682d38f6"}).then((data) => {

        res.json(data);
        console.log(' data is updated')


    }).catch((err) => {

        console.log(' data is not updated');
        res.json(err);

    });


    sampleschmaCtrl.updatename = (req,res)=>{

        sampleModel.findOneAndUpdate({"_id": "5a7c197a61a28737682d38f7"},{"username": "RaviPrasadReddy"}).then((data)=>{
            res.json(data);
            console.log("name  is updated ")
        }).catch((err)=>{
            res.json(err);
            console.log('name is not updated');        
        });
  }



}



// export the sampleschemactrl to route.js page to do the routing the url 

module.exports = sampleschmaCtrl;
