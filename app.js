//Model File System in node 
// var fs = require('fs'); 
//           fs.readFile("Sample.txt",function(error,data)
//           {
//                 console.log("Reading Data completed");
//      });

// http module with fs module and  page navigation
// var http = require('http');
// var fs = require('fs');
// function SendloginPage(res) {
//     fs.readFile("login.html", function (err, data) {
//         if (err) {
//             console.log('Login error Please Check');
//         }
//         else {
//             res.end(data.toString())
//             //            console.log(data.toString());
//         }
//         console.log("Reading Data completed");
//     });

// }
// function SendRegisterPage(res){
//         fs.readFile("register.html",function(err,data){
//             if(err){
//                 console.log('error in Page...')
//             }

//             else{
//                 res.end(data.toString());
//             }
//         })
// }



// var server = http.createServer(function (req, res) {
//     //res.end('Hello World');

//     if (req.url == '/r') {

//         res.end(`<html>
//             <body>
//             <h3>It is Working...</h3>
//             </body>
//         </html>`)

//     }


//     if (req.url == '/login') {

//         SendloginPage(res);


//     }

//     if(req.url == '/register'){

//         SendRegisterPage(res);
//     }

// });
// server.listen(5005, () => {
//     console.log("server running on port 5005");
// });
//////////////////////////////


//Using Express 

//////////////////////////



// importing the config.js file in app.js

var configaration = require('./config/config');


// importing the express-config.js file in app.js
 
var express = require('./config/express-config');


var db = require('./config/mongoose-config');

db();



let app = express();

app.listen(configaration.PORT,(err,data)=>{
    if(err){
        console.log(`PORT Number ${configaration.PORT} is Already Used `)
    }
    else{

        console.log(`PORT Number ${configaration.PORT} is Working.. `)

        
    }
});
