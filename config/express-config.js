
var express  = require('express');
var bodyParser = require('body-parser')
var app = express();

//export the routing functionality in app.js

module.exports  = function(){

    let app = express();
    


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())



    //To serve css and js files for the views.
    app.use(express.static('public'));
    
    


// set the view engine to ejs
app.set('view engine', 'ejs');


// import the routes.js here 

var routes = require('../app/routes');

routes(app);

return app;

}